var fs = require('fs');
var httpRequest = require("request");
var _ = require("underscore");

module.exports = function connectSSI(opt) {
  return function (req, res, next) {
    var content,
        includes,
        filename,
        remaining;

    if (req.url !== '/' && !req.url.match(/\.html/)) {
      return next();
    }

    if (req.url === '/') {
      filename = opt.root;
    } else {
      filename = req.url.slice(1);
    }

    content = fs.readFileSync(filename).toString();
    includes = content.match(/<!--\s*#include virtual=".*"\s*-->/g);

    if (includes === null) {
      return next();
    }

    remaining = includes.length;
    _.each(includes, function (include) {
      var url = opt.host + include.match(/<!--\s*#include virtual="(.*)"\s*-->/)[1];

      httpRequest(url, function (error, response, data) {
        var newInclude;

        if(error || response.statusCode !== 200) {
          console.log("ERROR including file " + url + ": " + error);
        }
        else {

          // if the URL starts with "//", we don't want to replace it, but when it
          // doesn't, we need to capture the first character after the / (e.g., if
          // it starts with "/css", newInclude would start with /ss if we did not
          // include the captured 'c' with '$1')
          newInclude = data
            .replace(/href="\/([^/])/g, "href=\"" + opt.host + "/$1")
            .replace(/src="\/([^/])/g, "src=\"" + opt.host + "/$1");

          content = content.replace(include, newInclude);
        }

        if (!--remaining) {
          res.send(content);
        }
      });
    });
  };
};
