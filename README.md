# connect-ssi

A node Connect middleware to support SSI-like functionality

## Usage

`connect-ssi.js` exports a single function which accepts a host url parameter
and returns a connect middleware.

When the middleware encounters the following in an html file:

```html
<!-- #include virtual="/test.ssi" -->
```

it will be replaced with the contents of `<host url>/test.ssi`. All relative
paths in the included file will updated to include the host url. Note only
virtual includes are supported.

### Installation

Add

```javascript
"connect-ssi": "git+https://bitbucket.org/nsidc/connect-ssi.git"
```

to the devDependencies section in your `package.json` file and run `npm
install`.

### Connect configuration

In your `Gruntfile.js` you must configure Connect to use middleware, for
example:

```javascript
connect: {
    server: {
        options: {
          hostname: '', // Setting host name to empty string allows port
                        // forwarding to work on VMs
          port: 3000,
          base: 'src',
          keepalive: true,
          middleware: function (connect, options) {
            var connectSSI = require('connect-ssi');
            return [
              connectSSI({
                host:'http://testhost.com',
                root:'index.html'
              }),
              connect.static(options.base)
            ];
          }
        }
    }
}
```

You can configure Express to use the middleware in your `server.js`:

```javascript
var express = require('express');
var connectSSI = require('connect-ssi');
var app = express();

app.use(connectSSI({
  root: 'index.html',
  host: 'http://qa.nsidc.org/'
}));
```

See
[https://github.com/gruntjs/grunt-contrib-connect](https://github.com/gruntjs/grunt-contrib-connect)
and
[http://expressjs.com/4x/api.html#middleware](http://expressjs.com/4x/api.html#middleware)
for more information about setting up middleware.
